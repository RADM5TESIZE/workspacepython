import pandas as pd
import pandas_datareader
import matplotlib.pyplot as plt
import seaborn as sns


file = pd.read_csv(r"C:\Users\muellerm\Desktop\workspace\train.csv")
df = pd.DataFrame(file)


features = pd.DataFrame(df.loc[:,'Feature_1':'Feature_25'])


corrdf = features.corr()
top_corr_features = corrdf.index


feature_corr = pd.DataFrame(features[top_corr_features].corr())

plt.figure()
g=sns.heatmap(feature_corr, annot=True,cmap="RdYlGn")
plt.show()
df1 = pd.DataFrame()
for column in features.columns:

    feature_1 = pd.DataFrame(df.loc[:,[column,'Ret_MinusTwo', 'Ret_MinusOne', 'Ret_2', 'Ret_3', 'Ret_4', 'Ret_5', 'Ret_6', 'Ret_7', 'Ret_8', 'Ret_9', 'Ret_10', 'Ret_11', 'Ret_12', 'Ret_13', 'Ret_14', 'Ret_15', 'Ret_16', 'Ret_17', 'Ret_18', 'Ret_19', 'Ret_20', 'Ret_21', 'Ret_22', 'Ret_23', 'Ret_24', 'Ret_25', 'Ret_26', 'Ret_27', 'Ret_28', 'Ret_29', 'Ret_30', 'Ret_31', 'Ret_32', 'Ret_33', 'Ret_34', 'Ret_35', 'Ret_36', 'Ret_37', 'Ret_38', 'Ret_39', 'Ret_40', 'Ret_41', 'Ret_42', 'Ret_43', 'Ret_44', 'Ret_45', 'Ret_46', 'Ret_47', 'Ret_48', 'Ret_49', 'Ret_50', 'Ret_51', 'Ret_52', 'Ret_53', 'Ret_54', 'Ret_55', 'Ret_56', 'Ret_57', 'Ret_58', 'Ret_59', 'Ret_60', 'Ret_61', 'Ret_62', 'Ret_63', 'Ret_64', 'Ret_65', 'Ret_66', 'Ret_67', 'Ret_68', 'Ret_69', 'Ret_70', 'Ret_71', 'Ret_72', 'Ret_73', 'Ret_74', 'Ret_75', 'Ret_76', 'Ret_77', 'Ret_78', 'Ret_79', 'Ret_80', 'Ret_81', 'Ret_82', 'Ret_83', 'Ret_84', 'Ret_85', 'Ret_86', 'Ret_87', 'Ret_88', 'Ret_89', 'Ret_90', 'Ret_91', 'Ret_92', 'Ret_93', 'Ret_94', 'Ret_95', 'Ret_96', 'Ret_97', 'Ret_98', 'Ret_99', 'Ret_100', 'Ret_101', 'Ret_102', 'Ret_103', 'Ret_104', 'Ret_105', 'Ret_106', 'Ret_107', 'Ret_108', 'Ret_109', 'Ret_110', 'Ret_111', 'Ret_112', 'Ret_113', 'Ret_114', 'Ret_115', 'Ret_116', 'Ret_117', 'Ret_118', 'Ret_119', 'Ret_120', 'Ret_121', 'Ret_122', 'Ret_123', 'Ret_124', 'Ret_125', 'Ret_126', 'Ret_127', 'Ret_128', 'Ret_129', 'Ret_130', 'Ret_131', 'Ret_132', 'Ret_133', 'Ret_134', 'Ret_135', 'Ret_136', 'Ret_137', 'Ret_138', 'Ret_139', 'Ret_140', 'Ret_141', 'Ret_142', 'Ret_143', 'Ret_144', 'Ret_145', 'Ret_146', 'Ret_147', 'Ret_148', 'Ret_149', 'Ret_150', 'Ret_151', 'Ret_152', 'Ret_153', 'Ret_154', 'Ret_155', 'Ret_156', 'Ret_157', 'Ret_158', 'Ret_159', 'Ret_160', 'Ret_161', 'Ret_162', 'Ret_163', 'Ret_164', 'Ret_165', 'Ret_166', 'Ret_167', 'Ret_168', 'Ret_169', 'Ret_170', 'Ret_171', 'Ret_172', 'Ret_173', 'Ret_174', 'Ret_175', 'Ret_176', 'Ret_177', 'Ret_178', 'Ret_179', 'Ret_180', 'Ret_PlusOne', 'Ret_PlusTwo', 'Weight_Intraday', 'Weight_Daily']])
    corrfeature_1 = feature_1.corr()
    top_corr_feature_1 = corrfeature_1.index
    feature_1_corr = pd.DataFrame(feature_1[top_corr_feature_1].corr())

    feature_1_corr = feature_1_corr.replace(1,0)

    temp = pd.DataFrame(feature_1_corr)

    feature_1_corr = feature_1_corr[(feature_1_corr >= 0.3).any(axis=1)]
    temp = temp[(temp <= -0.3).any(axis=1)]
    feature_1_corr = feature_1_corr.T
    temp = temp.T

    feature_1_corr = feature_1_corr[(feature_1_corr >= 0.3).any(axis=1)]
    temp = temp[(temp <= -0.3).any(axis=1)]

    temp = temp.dropna(axis=1)
    feature_1_corr = feature_1_corr.dropna(axis=1)

    df1 = pd.concat([df1,feature_1_corr]).drop_duplicates()
    df1 = pd.concat([df1, temp]).drop_duplicates()
    print(df1)
print(df1)
# df1 = df1.dropna(axis=1)
#
# temp = pd.DataFrame(df1)
# temp = temp.T
# df1 = df1.T
# df1 = df1[(df1 >= 0.3).any(axis=1)]
# temp = temp[(temp <= -0.3).any(axis=1)]
# df1 = pd.concat([df1,temp])
df1 = df1.drop_duplicates()
plt.figure(figsize=df1.shape)
plt.grid(False)
g=sns.heatmap(df1, annot=True,cmap="RdYlGn")
plt.show()